cmake_minimum_required(VERSION 3.11)
project(awake_discord)
include(FetchContent)

set(CMAKE_CXX_STANDARD 20)

set(BITFLAGS_BUILD_TESTS OFF)
set(BITFLAGS_BUILD_SAMPLES OFF)
set(CPR_BUILD_TESTS OFF)
set(CPR_BUILD_TESTS_SSL OFF)

FetchContent_Declare(
        simple-websocket-server
        GIT_REPOSITORY https://gitlab.com/eidheim/Simple-WebSocket-Server.git
)
FetchContent_MakeAvailable(simple-websocket-server)

FetchContent_Declare(
        curl
        GIT_REPOSITORY https://github.com/curl/curl.git
)
FetchContent_MakeAvailable(curl)

FetchContent_Declare(
        cpr
        GIT_REPOSITORY https://github.com/whoshuu/cpr.git
)
FetchContent_MakeAvailable(cpr)

FetchContent_Declare(
        json
        GIT_REPOSITORY https://github.com/nlohmann/json.git
        GIT_TAG origin/master
)
FetchContent_MakeAvailable(json)

FetchContent_Declare(
        bitflags
        GIT_REPOSITORY https://github.com/m-peko/bitflags.git
        GIT_TAG 58ab337
)
FetchContent_MakeAvailable(bitflags)

file(GLOB awake-discord-srcs "awake-discord/*.cpp")

add_library(awake-discord SHARED ${awake-discord-srcs})

target_include_directories(awake-discord PUBLIC include)

target_link_libraries(
        awake-discord
        PUBLIC cpr
        PUBLIC nlohmann_json
        PUBLIC bitflags
)