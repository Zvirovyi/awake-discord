#include <awake-discord/string-enums.h>

#include <stdexcept>

namespace AwakeDiscord {

    const std::string EmbedType_Values::values[] { "rich", "image", "video", "gifv", "article", "link"};

    EmbedType EmbedType_Values::fromString(const std::string &str) {
        for (int i = 0; i < std::size(values); i++) {
            if (str == values[i]) {
                return EmbedType(i);
            }
        }
        throw std::range_error("Value \"" + str + "\" not found in EmbedType_Values");
    }

    const std::string PresenceUpdateStatus_Values::values[] { "idle", "dnd", "online", "offline" };

    PresenceUpdateStatus PresenceUpdateStatus_Values::fromString(const std::string &str) {
        for (int i = 0; i < std::size(values); i++) {
            if (str == values[i]) {
                return PresenceUpdateStatus(i);
            }
        }
        throw std::range_error("Value \"" + str + "\" not found in PresenceUpdateStatus_Values");
    }

    const std::string ClientStatusStatus_Values::values[] { "online", "idle", "dnd" };

    ClientStatusStatus ClientStatusStatus_Values::fromString(const std::string &str) {
        for (int i = 0; i < std::size(values); i++) {
            if (str == values[i]) {
                return ClientStatusStatus(i);
            }
        }
        throw std::range_error("Value \"" + str + "\" not found in ClientStatusStatus_Values");
    }

    const std::string GuildFeatureFlags_Values::values[] { "ANIMATED_ICON", "BANNER", "COMMERCE", "COMMUNITY", "DISCOVERABLE", "FEATURABLE", "INVITE_SPLASH", "MEMBER_VERIFICATION_GATE_ENABLED", "NEWS", "PARTNERED", "PREVIEW_ENABLED", "VANITY_URL", "VERIFIED", "VIP_REGIONS", "WELCOME_SCREEN_ENABLED", "TICKETED_EVENTS_ENABLED", "MONETIZATION_ENABLED", "MORE_STICKERS" };

    GuildFeatureFlags GuildFeatureFlags_Values::fromStringsVector(const std::vector<std::string> &strs) {
        GuildFeatureFlags res = 0;
        for (const std::string &str : strs) {
            for (int i = 0; i < std::size(values); i++) {
                if (str == values[i]) {
                    res |= 1 << i;
                }
            }
        }
        return res;
    }

    std::vector<std::string> GuildFeatureFlags_Values::toStringsVector(GuildFeatureFlags input) {
        std::vector<std::string> res;
        for (int i = 0; i < std::size(values); i++) {
            if (input.bits() & 1 << i) {
                res.push_back(values[i]);
            }
        }
        return res;
    }

    const std::string IntegrationType_Values::values[] { "twitch", "youtube", "discord" };

    IntegrationType IntegrationType_Values::fromString(const std::string &str) {
        for (int i = 0; i < std::size(values); i++) {
            if (str == values[i]) {
                return IntegrationType(i);
            }
        }
        throw std::range_error("Value \"" + str + "\" not found in IntegrationType_Values");
    }

    const std::string AuditLogChangeKey_Values::values[] { "name", "description", "icon_hash", "splash_hash", "discovery_splash_hash", "banner_hash", "owner_id", "region", "preferred_locale", "afk_channel_id", "afk_timeout", "rules_channel_id", "public_updates_channel_id", "mfa_level", "verification_level", "explicit_content_filter", "default_message_notifications", "vanity_url_code", "$add", "$remove", "prune_delete_days", "widget_enabled", "widget_channel_id", "system_channel_id", "position", "topic", "bitrate", "permission_overwrites", "nsfw", "application_id", "rate_limit_per_user", "permissions", "color", "hoist", "mentionable", "allow", "deny", "code", "channel_id", "inviter_id", "max_uses", "uses", "max_age", "temporary", "deaf", "mute", "nick", "avatar_hash", "id", "type", "enable_emoticons", "expire_behavior", "expire_grace_period", "user_limit", "privacy_level" };

    AuditLogChangeKey AuditLogChangeKey_Values::fromString(const std::string &str) {
        for (int i = 0; i < std::size(values); i++) {
            if (str == values[i]) {
                return AuditLogChangeKey(i);
            }
        }
        //TODO discord can't make right documentation, so hotfix
        //throw std::range_error("Value \"" + str + "\" not found in AuditLogChangeKey_Values");
        return AuditLogChangeKey(std::size(values));
    }

    const std::string ConnectionService_Values::values[] { "twitch", "youtube" };

    ConnectionService ConnectionService_Values::fromString(const std::string &str) {
        for (int i = 0; i < std::size(values); i++) {
            if (str == values[i]) {
                return ConnectionService(i);
            }
        }
        throw std::range_error("Value \"" + str + "\" not found in ConnectionService_Values");
    }

    const std::string AllowedMentionTypeFlags_Values::values[] { "roles", "users", "everyone" };

    AllowedMentionTypeFlags AllowedMentionTypeFlags_Values::fromStringsVector(const std::vector<std::string> &strs) {
        AllowedMentionTypeFlags res = 0;
        for (const std::string &str : strs) {
            for (int i = 0; i < std::size(values); i++) {
                if (str == values[i]) {
                    res |= 1 << i;
                }
            }
        }
        return res;
    }

    std::vector<std::string> AllowedMentionTypeFlags_Values::toStringsVector(AllowedMentionTypeFlags input) {
        std::vector<std::string> res;
        for (int i = 0; i < std::size(values); i++) {
            if (input.bits() & 1 << i) {
                res.push_back(values[i]);
            }
        }
        return res;
    }

    const std::string OptionalAuditEntryInfoType_Values::values[] { "0", "1" };

    OptionalAuditEntryInfoType OptionalAuditEntryInfoType_Values::fromString(const std::string &str) {
        for (int i = 0; i < std::size(values); i++) {
            if (str == values[i]) {
                return OptionalAuditEntryInfoType(i);
            }
        }
        throw std::range_error("Value \"" + str + "\" not found in OptionalAuditEntryInfoType_Values");
    }

}