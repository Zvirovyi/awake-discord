#include <awake-discord/bot.h>
#include <awake-discord/constants.h>

#include <cpr/cpr.h>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    Bot::Bot(const std::string &token) : authorization { "Authorization", "Bot " + token } { }

    AuditLog Bot::getGuildAuditLog(const std::string &guildID, std::optional<std::string> user_id, std::optional<AuditLogEvent> action_type, std::optional<std::string> before, std::optional<int> limit) {
        cpr::Parameters params {};
        if (user_id.has_value()) {
            params.Add( { "user_id", user_id.value() } );
        }
        if (action_type.has_value()) {
            params.Add( { "action_type", std::to_string((int)action_type.value()) } );
        }
        if (before.has_value()) {
            params.Add( { "before", before.value() } );
        }
        if (limit.has_value()) {
            params.Add( { "limit", std::to_string(limit.value()) } );
        }
        cpr::Response response = cpr::Get(cpr::Url { DiscordAPIUrl + "/guilds/" + guildID + "/audit-logs" }, params, cpr::Header { authorization });
        return { nlohmann::json::parse(response.text) };
    }

    User Bot::getCurrentUser() {
        cpr::Response response = cpr::Get(cpr::Url { DiscordAPIUrl + "/users/@me" }, cpr::Header { authorization });
        return { nlohmann::json::parse(response.text) };
    }

    WelcomeScreen Bot::getGuildWelcomeScreen(const std::string &guildID) {
        cpr::Response response = cpr::Get(cpr::Url { DiscordAPIUrl + "/guilds/" + guildID + "/welcome-screen" }, cpr::Header { authorization });
        return { nlohmann::json::parse(response.text) };
    }

}