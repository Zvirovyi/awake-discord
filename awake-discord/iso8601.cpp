#include <awake-discord/iso8601.h>

namespace AwakeDiscord {

    //Example - 2021-06-26T10:08:38.007000+00:00.
    //https://en.wikipedia.org/wiki/ISO_8601
    time_t ISO8601::fromTimestamp(std::string str) {
        tm t;
        str.erase(str.size() - 3);
        strptime(str.c_str(), "%Y-%m-%dT%H:%M:%S.%f%z", &t);
        return mktime(&t);
    }

}