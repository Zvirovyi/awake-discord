#pragma once

#include <awake-discord/guild-member.h>
#include <awake-discord/channel-mention.h>
#include <awake-discord/attachment.h>
#include <awake-discord/embed.h>
#include <awake-discord/reaction.h>
#include <awake-discord/application.h>
#include <awake-discord/message-activity.h>
#include <awake-discord/message-reference.h>
#include <awake-discord/message-sticker.h>
#include <awake-discord/message-interaction.h>
#include <awake-discord/component.h>
#include <awake-discord/channel.h>

#include <variant>
#include <memory>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#message-object-message-structure
    struct Message {

        std::string id;

        std::string channel_id;

        std::optional<std::string> guild_id;

        User author;

        std::optional<GuildMember> member;

        std::string content;

        time_t timestamp;

        std::optional<time_t> edited_timestamp;

        bool tts;

        bool mention_everyone;

        std::vector<User> mentions;

        std::vector<std::string> mention_roles;

        std::vector<ChannelMention> mention_channels;

        std::vector<Attachment> attachments;

        std::vector<Embed> embeds;

        std::vector<Reaction> reactions;

        std::optional<std::variant<int, std::string>> nonce;

        bool pinned;

        std::optional<std::string> webhook_id;

        MessageType type;

        std::optional<MessageActivity> activity;

        std::optional<Application> application;

        std::optional<std::string> application_id;

        std::optional<MessageReference> message_reference;

        std::optional<MessageFlags> flags;

        std::vector<MessageSticker> stickers;

        std::unique_ptr<Message> referenced_message;

        std::optional<MessageInteraction> interaction;

        std::optional<Channel> thread;

        std::vector<Component> components;

        Message() = default;

        Message(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            channel_id = json["channel_id"].get<std::string>();
            if (json.contains("guild_id")) {
                guild_id = json["guild_id"].get<std::string>();
            }
            author = User(json["author"]);
            if (json.contains("member")) {
                member = GuildMember(json["member"]);
            }
            content = json["content"].get<std::string>();
            timestamp = ISO8601::fromTimestamp(json["timestamp"].get<std::string>());
            if (json.contains("edited_timestamp")) {
                edited_timestamp = ISO8601::fromTimestamp(json["edited_timestamp"].get<std::string>());
            }
            tts = json["tts"].get<bool>();
            mention_everyone = json["mention_everyone"].get<bool>();
            for (const nlohmann::json &jsonEl : json["mentions"]) {
                mentions.emplace_back(jsonEl);
            }
            for (const nlohmann::json &jsonEl : json["mention_roles"]) {
                mention_roles.push_back(jsonEl.get<std::string>());
            }
            if (json.contains("mention_channels")) {
                for (const nlohmann::json &jsonEl : json["mention_channels"]) {
                    mention_channels.emplace_back(jsonEl);
                }
            }
            for (const nlohmann::json &jsonEl : json["attachments"]) {
                attachments.emplace_back(jsonEl);
            }
            for (const nlohmann::json &jsonEl : json["embeds"]) {
                embeds.emplace_back(jsonEl);
            }
            if (json.contains("reactions")) {
                for (const nlohmann::json &jsonEl : json["reactions"]) {
                    reactions.emplace_back(jsonEl);
                }
            }
            if (json.contains("nonce")) {
                if (json["nonce"].is_number_integer()) {
                    nonce = json["nonce"].get<int>();
                } else {
                    nonce = json["nonce"].get<std::string>();
                }
            }
            pinned = json["pinned"].get<bool>();
            if (json.contains("webhook_id")) {
                webhook_id = json["webhook_id"].get<std::string>();
            }
            type = MessageType(json["type"].get<int>());
            if (json.contains("activity")) {
                activity = MessageActivity(json["activity"]);
            }
            if (json.contains("application")) {
                application = Application(json["application"]);
            }
            if (json.contains("application_id")) {
                application_id = json["application_id"].get<std::string>();
            }
            if (json.contains("message_reference")) {
                message_reference = MessageReference(json["message_reference"]);
            }
            if (json.contains("flags")) {
                flags = MessageFlags(json["flags"].get<int>());
            }
            if (json.contains("stickers")) {
                for (const nlohmann::json &jsonEl : json["stickers"]) {
                    stickers.emplace_back(jsonEl);
                }
            }
            if (json.contains("referenced_message") && !json["referenced_message"].is_null()) {
                referenced_message = std::make_unique<Message>(Message(json["referenced_message"]));
            }
            if (json.contains("interaction")) {
                interaction = MessageInteraction(json["interaction"]);
            }
            if (json.contains("thread")) {
                thread = Channel(json["thread"]);
            }
            if (json.contains("components")) {
                for (const nlohmann::json &jsonEl : json["components"]) {
                    components.emplace_back(jsonEl);
                }
            }
        }

    };

}