#pragma once

#include <awake-discord/enums.h>

#include <optional>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#message-object-message-activity-structure
    struct MessageActivity {

        MessageActivityType type;

        std::optional<std::string> party_id;

        MessageActivity() = default;

        MessageActivity(const nlohmann::json &json) {
            type = MessageActivityType(json["type"].get<int>());
            if (json.contains("party_id")) {
                party_id = json["party_id"].get<std::string>();
            }
        }

    };

}