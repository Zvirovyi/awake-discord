#pragma once

#include <awake-discord/guild-member.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/voice#voice-state-object-voice-state-structure
    struct VoiceState {

        std::optional<std::string> guild_id;

        std::optional<std::string> channel_id;

        std::string user_id;

        std::optional<GuildMember> member;

        std::string session_id;

        bool deaf;

        bool mute;

        bool self_deaf;

        bool self_mute;

        std::optional<bool> self_stream;

        bool self_video;

        bool suppress;

        std::optional<time_t> request_to_speak_timestamp;

        VoiceState() = default;

        VoiceState(const nlohmann::json &json) {
            if (json.contains("guild_id")) {
                guild_id = json["guild_id"].get<std::string>();
            }
            if (!json["channel_id"].is_null()) {
                channel_id = json["channel_id"].get<std::string>();
            }
            user_id = json["user_id"].get<std::string>();
            if (json.contains("member")) {
                member = GuildMember(json["member"]);
            }
            session_id = json["session_id"].get<std::string>();
            deaf = json["deaf"].get<bool>();
            mute = json["mute"].get<bool>();
            self_deaf = json["self_deaf"].get<bool>();
            self_mute = json["self_mute"].get<bool>();
            if (json.contains("self_stream")) {
                self_stream = json["self_stream"].get<bool>();
            }
            self_video = json["self_video"].get<bool>();
            suppress = json["suppress"].get<bool>();
            if (!json["request_to_speak_timestamp"].is_null()) {
                request_to_speak_timestamp = ISO8601::fromTimestamp(json["request_to_speak_timestamp"].get<std::string>());
            }
        }

    };

}