#pragma once

#include <awake-discord/audit-log.h>
#include <awake-discord/user.h>
#include <awake-discord/message.h>
#include <awake-discord/welcome-screen.h>

namespace AwakeDiscord {

    class Bot {

    private:

        std::pair<std::string, std::string> authorization;

    public:

        Bot(const std::string & token);

        AuditLog getGuildAuditLog(const std::string &guildID, std::optional<std::string> user_id = std::nullopt, std::optional<AuditLogEvent> action_type = std::nullopt, std::optional<std::string> before = std::nullopt, std::optional<int> limit = std::nullopt);

        User getCurrentUser();

        WelcomeScreen getGuildWelcomeScreen(const std::string &guildID);

    };

}