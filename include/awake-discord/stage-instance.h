#pragma once

#include <awake-discord/enums.h>

#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/stage-instance#stage-instance-object-stage-instance-structure
    struct StageInstance {

        std::string id;

        std::string guild_id;

        std::string channel_id;

        std::string topic;

        PrivacyLevel privacy_level;

        bool discoverable_disabled;

        StageInstance() = default;

        StageInstance(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            guild_id = json["guild_id"].get<std::string>();
            channel_id = json["channel_id"].get<std::string>();
            topic = json["topic"].get<std::string>();
            privacy_level = PrivacyLevel(json["privacy_level"].get<int>());
            discoverable_disabled = json["discoverable_disabled"].get<bool>();
        }

    };

}