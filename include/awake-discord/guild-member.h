#pragma once

#include <awake-discord/user.h>
#include <awake-discord/iso8601.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/guild#guild-member-object-guild-member-structure
    struct GuildMember {

        std::optional<User> user;

        std::optional<std::string> nick;

        std::vector<std::string> roles;

        time_t joined_at;

        std::optional<time_t> premium_since;

        bool deaf;

        bool mute;

        std::optional<bool> pending;

        std::optional<PermissionFlags> permissions;

        GuildMember() = default;

        GuildMember(const nlohmann::json &json) {
            if (json.contains("user")) {
                user = User(json["user"]);
            }
            if (json.contains("nick") && !json["nick"].is_null()) {
                nick = json["nick"].get<std::string>();
            }
            for (const nlohmann::json &jsonEl : json["roles"]) {
                roles.push_back(jsonEl.get<std::string>());
            }
            joined_at = ISO8601::fromTimestamp(json["joined_at"].get<std::string>());
            if (json.contains("premium_since") && !json["premium_since"].is_null()) {
                premium_since = ISO8601::fromTimestamp(json["premium_since"].get<std::string>());
            }
            deaf = json["deaf"].get<bool>();
            mute = json["mute"].get<bool>();
            if (json.contains("pending")) {
                pending = json["pending"].get<bool>();
            }
            if (json.contains("permissions")) {
                permissions = PermissionFlags(std::stoll(json["permissions"].get<std::string>()));
            }
        }

    };

}