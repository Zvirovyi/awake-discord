#pragma once

#include <bitflags/bitflags.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#channel-object-channel-types
    enum class ChannelType {
        GUILD_TEXT,
        DM,
        GUILD_VOICE,
        GROUP_DM,
        GUILD_CATEGORY,
        GUILD_NEWS,
        GUILD_STORE,
        GUILD_NEWS_THREAD = 10,
        GUILD_PUBLIC_THREAD,
        GUILD_PRIVATE_THREAD,
        GUILD_STAGE_VOICE
    };

    //https://discord.com/developers/docs/resources/user#user-object-user-flags
    BITFLAGS(UserFlags, int,
            BITFLAG(0, None)
            BITFLAG(1 << 0, DiscordEmployee)
            BITFLAG(1 << 1, PartneredServerOwner)
            BITFLAG(1 << 2, HypeSquadEvents)
            BITFLAG(1 << 3, BugHunterLevel1)
            BITFLAG(1 << 6, HouseBravery)
            BITFLAG(1 << 7, HouseBrilliance)
            BITFLAG(1 << 8, HouseBalance)
            BITFLAG(1 << 9, EarlySupporter)
            BITFLAG(1 << 10, TeamUser)
            BITFLAG(1 << 14, BugHunterLevel2)
            BITFLAG(1 << 16, VerifiedBot)
            BITFLAG(1 << 17, EarlyVerifiedBotDeveloper)
            BITFLAG(1 << 18, DiscordCertifiedModerator)
    )

    //https://discord.com/developers/docs/resources/user#user-object-premium-types
    enum class PremiumType {
        None,
        NitroClassic,
        Nitro
    };

    //https://discord.com/developers/docs/resources/channel#message-object-message-types
    enum class MessageType {
        DEFAULT,
        RECIPIENT_ADD,
        RECIPIENT_REMOVE,
        CALL,
        CHANNEL_NAME_CHANGE,
        CHANNEL_ICON_CHANGE,
        CHANNEL_PINNED_MESSAGE,
        GUILD_MEMBER_JOIN,
        USER_PREMIUM_GUILD_SUBSCRIPTION,
        USER_PREMIUM_GUILD_SUBSCRIPTION_TIER_1,
        USER_PREMIUM_GUILD_SUBSCRIPTION_TIER_2,
        USER_PREMIUM_GUILD_SUBSCRIPTION_TIER_3,
        CHANNEL_FOLLOW_ADD,
        GUILD_DISCOVERY_DISQUALIFIED = 14,
        GUILD_DISCOVERY_REQUALIFIED,
        GUILD_DISCOVERY_GRACE_PERIOD_INITIAL_WARNING,
        GUILD_DISCOVERY_GRACE_PERIOD_FINAL_WARNING,
        THREAD_CREATED,
        REPLY,
        APPLICATION_COMMAND,
        THREAD_STARTER_MESSAGE,
        GUILD_INVITE_REMINDER
    };

    //https://discord.com/developers/docs/resources/channel#message-object-message-activity-types
    enum class MessageActivityType {
        JOIN = 1,
        SPECTATE,
        LISTEN,
        JOIN_REQUEST = 5
    };

    //https://discord.com/developers/docs/topics/teams#data-models-membership-state-enum
    enum class MembershipState {
        INVITED = 1,
        ACCEPTED
    };

    //https://discord.com/developers/docs/resources/application#application-object-application-flags
    BITFLAGS(ApplicationFlags, int,
            BITFLAG(1 << 12, GATEWAY_PRESENCE)
            BITFLAG(1 << 13, GATEWAY_PRESENCE_LIMITED)
            BITFLAG(1 << 14, GATEWAY_GUILD_MEMBERS)
            BITFLAG(1 << 15, GATEWAY_GUILD_MEMBERS_LIMITED)
            BITFLAG(1 << 16, VERIFICATION_PENDING_GUILD_LIMIT)
            BITFLAG(1 << 17, EMBEDDED)
    )

    //https://discord.com/developers/docs/resources/channel#message-object-message-flags
    BITFLAGS(MessageFlags, int,
            BITFLAG(1 << 0, CROSSPOSTED)
            BITFLAG(1 << 1, IS_CROSSPOST)
            BITFLAG(1 << 2, SUPPRESS_EMBEDS)
            BITFLAG(1 << 3, SOURCE_MESSAGE_DELETED)
            BITFLAG(1 << 4, URGENT)
            BITFLAG(1 << 5, HAS_THREAD)
            BITFLAG(1 << 6, EPHEMERAL)
            BITFLAG(1 << 7, LOADING)
    )

    //https://discord.com/developers/docs/resources/channel#message-object-message-sticker-format-types
    enum class MessageStickerFormatType {
        PNG = 1,
        apnG,
        LOTTIE
    };

    //https://discord.com/developers/docs/interactions/slash-commands#interaction-object-interaction-request-type
    enum class InteractionRequestType {
        Ping = 1,
        ApplicationCommand,
        MessageComponent
    };

    //https://discord.com/developers/docs/interactions/slash-commands#interaction-response-object-interaction-callback-type
    enum class InteractionCallbackType {
        Pong = 1,
        ChannelMessageWithSource = 4,
        DeferredChannelMessageWithSource,
        DeferredUpdateMessage,
        UpdateMessage
    };

    //https://discord.com/developers/docs/interactions/message-components#component-types
    enum class ComponentType {
        ActionRow = 1,
        Button
    };

    //https://discord.com/developers/docs/interactions/message-components#buttons-button-styles
    enum class ButtonStyle {
        Primary = 1,
        Secondary,
        Success,
        Danger,
        Link
    };

    //https://discord.com/developers/docs/resources/channel#overwrite-object-overwrite-structure
    enum class OverwriteType {
        Role,
        Member
    };

    //https://discord.com/developers/docs/resources/channel#channel-object-video-quality-modes
    enum class VideoQualityMode {
        AUTO = 1,
        FULL
    };

    //https://discord.com/developers/docs/resources/webhook#webhook-object-webhook-types
    enum class WebhookType {
        Incoming = 1,
        ChannelFollower,
        Application
    };

    //https://discord.com/developers/docs/resources/stage-instance#stage-instance-object-privacy-level
    enum class PrivacyLevel {
        PUBLIC = 1,
        GUILD_ONLY
    };

    //https://discord.com/developers/docs/topics/gateway#activity-object-activity-flags
    BITFLAGS(ActivityFlags, int,
             BITFLAG(1 << 0, INSTANCE)
             BITFLAG(1 << 1, JOIN)
             BITFLAG(1 << 2, SPECTATE)
             BITFLAG(1 << 3, JOIN_REQUEST)
             BITFLAG(1 << 4, SYNC)
             BITFLAG(1 << 5, PLAY)
    )

    //https://discord.com/developers/docs/topics/gateway#activity-object-activity-types
    enum class ActivityType {
        Game,
        Streaming,
        Listening,
        Watching,
        Custom,
        Completing
    };

    //https://discord.com/developers/docs/topics/permissions#permissions-bitwise-permission-flags
    BITFLAGS(PermissionFlags, long long,
             BITFLAG(1 << 0, CREATE_INSTANT_INVITE)
             BITFLAG(1 << 1, KICK_MEMBERS)
             BITFLAG(1 << 2, BAN_MEMBERS)
             BITFLAG(1 << 3, ADMINISTRATOR)
             BITFLAG(1 << 4, MANAGE_CHANNELS)
             BITFLAG(1 << 5, MANAGE_GUILD)
             BITFLAG(1 << 6, ADD_REACTIONS)
             BITFLAG(1 << 7, VIEW_AUDIT_LOG)
             BITFLAG(1 << 8, PRIORITY_SPEAKER)
             BITFLAG(1 << 9, STREAM)
             BITFLAG(1 << 10, VIEW_CHANNEL)
             BITFLAG(1 << 11, SEND_MESSAGES)
             BITFLAG(1 << 12, SEND_TTS_MESSAGES)
             BITFLAG(1 << 13, MANAGE_MESSAGES)
             BITFLAG(1 << 14, EMBED_LINKS)
             BITFLAG(1 << 15, ATTACH_FILES)
             BITFLAG(1 << 16, READ_MESSAGE_HISTORY)
             BITFLAG(1 << 17, MENTION_EVERYONE)
             BITFLAG(1 << 18, USE_EXTERNAL_EMOJIS)
             BITFLAG(1 << 19, VIEW_GUILD_INSIGHTS)
             BITFLAG(1 << 20, CONNECT)
             BITFLAG(1 << 21, SPEAK)
             BITFLAG(1 << 22, MUTE_MEMBERS)
             BITFLAG(1 << 23, DEAFEN_MEMBERS)
             BITFLAG(1 << 24, MOVE_MEMBERS)
             BITFLAG(1 << 25, USE_VAD)
             BITFLAG(1 << 26, CHANGE_NICKNAME)
             BITFLAG(1 << 27, MANAGE_NICKNAMES)
             BITFLAG(1 << 28, MANAGE_ROLES)
             BITFLAG(1 << 29, MANAGE_WEBHOOKS)
             BITFLAG(1 << 30, MANAGE_EMOJIS)
             BITFLAG(1 << 31, USE_SLASH_COMMANDS)
             BITFLAG(1l << 32, REQUEST_TO_SPEAK)
             BITFLAG(1l << 34, MANAGE_THREADS)
             BITFLAG(1l << 35, USE_PUBLIC_THREADS)
             BITFLAG(1l << 36, USE_PRIVATE_THREADS)
    )

    //https://discord.com/developers/docs/resources/guild#guild-object-verification-level
    enum class VerificationLevel {
        NONE,
        LOW,
        MEDIUM,
        HIGH,
        VERY_HIGH
    };

    //https://discord.com/developers/docs/resources/guild#guild-object-default-message-notification-level
    enum class DefaultMessageNotificationLevel {
        ALL_MESSAGES,
        ONLY_MENTIONS
    };

    //https://discord.com/developers/docs/resources/guild#guild-object-explicit-content-filter-level
    enum class ExplicitContentFilterLevel {
        DISABLED,
        MEMBERS_WITHOUT_ROLES,
        ALL_MEMBERS
    };

    //https://discord.com/developers/docs/resources/guild#guild-object-mfa-level
    enum class MFALevel {
        NONE,
        ELEVATED
    };

    //https://discord.com/developers/docs/resources/guild#guild-object-system-channel-flags
    BITFLAGS(SystemChannelFlags, int,
             BITFLAG(1 << 0, SUPPRESS_JOIN_NOTIFICATIONS)
             BITFLAG(1 << 1, SUPPRESS_PREMIUM_SUBSCRIPTIONS)
             BITFLAG(1 << 2, SUPPRESS_GUILD_REMINDER_NOTIFICATIONS)
    )

    //https://discord.com/developers/docs/resources/guild#guild-object-premium-tier
    enum class PremiumTier {
        NONE,
        TIER_1,
        TIER_2,
        TIER_3
    };

    //https://discord.com/developers/docs/resources/guild#guild-object-guild-nsfw-level
    enum class GuildNSFWLevel {
        DEFAULT,
        EXPLICIT,
        SAFE,
        AGE_RESTRICTED
    };

    //https://discord.com/developers/docs/resources/guild#integration-object-integration-expire-behaviors
    enum class IntegrationExpireBehavior {
        RemoveRole,
        Kick
    };

    //https://discord.com/developers/docs/resources/audit-log#audit-log-entry-object-audit-log-events
    enum class AuditLogEvent {
        GUILD_UPDATE = 1,
        CHANNEL_CREATE = 10,
        CHANNEL_UPDATE,
        CHANNEL_DELETE,
        CHANNEL_OVERWRITE_CREATE,
        CHANNEL_OVERWRITE_UPDATE,
        CHANNEL_OVERWRITE_DELETE,
        MEMBER_KICK = 20,
        MEMBER_PRUNE,
        MEMBER_BAN_ADD,
        MEMBER_BAN_REMOVE,
        MEMBER_UPDATE,
        MEMBER_ROLE_UPDATE,
        MEMBER_MOVE,
        MEMBER_DISCONNECT,
        BOT_ADD,
        ROLE_CREATE = 30,
        ROLE_UPDATE,
        ROLE_DELETE,
        INVITE_CREATE = 40,
        INVITE_UPDATE,
        INVITE_DELETE,
        WEBHOOK_CREATE = 50,
        WEBHOOK_UPDATE,
        WEBHOOK_DELETE,
        EMOJI_CREATE = 60,
        EMOJI_UPDATE,
        EMOJI_DELETE,
        MESSAGE_DELETE = 72,
        MESSAGE_BULK_DELETE,
        MESSAGE_PIN,
        MESSAGE_UNPIN,
        INTEGRATION_CREATE = 80,
        INTEGRATION_UPDATE,
        INTEGRATION_DELETE,
        STAGE_INSTANCE_CREATE,
        STAGE_INSTANCE_UPDATE,
        STAGE_INSTANCE_DELETE
    };

    //https://discord.com/developers/docs/resources/user#connection-object-visibility-types
    enum class Visibility {
        None,
        Everyone
    };

    //https://discord.com/developers/docs/resources/invite#invite-object-invite-target-types
    enum class InviteTargetType {
        STREAM = 1,
        EMBEDDED_APPLICATION
    };

}