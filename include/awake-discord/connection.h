#pragma once

#include <awake-discord/integration.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/user#connection-object-connection-structure
    struct Connection {

        std::string id;

        std::string name;

        ConnectionService type;

        std::optional<bool> revoked;

        std::vector<Integration> integrations;

        bool verified;

        bool friend_sync;

        bool show_activity;

        Visibility visibility;

        Connection() = default;

        Connection(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            name = json["name"].get<std::string>();
            type = ConnectionService_Values::fromString(json["type"].get<std::string>());
            if (json.contains("revoked")) {
                revoked = json["revoked"].get<bool>();
            }
            if (json.contains("integrations")) {
                for (const nlohmann::json &jsonEl : json["integrations"]) {
                    integrations.emplace_back(jsonEl);
                }
            }
            verified = json["verified"].get<bool>();
            friend_sync = json["friend_sync"].get<bool>();
            show_activity = json["show_activity"].get<bool>();
            visibility = Visibility(json["visibility"].get<int>());
        }

    };

}