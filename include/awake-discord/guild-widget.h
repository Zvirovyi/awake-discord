#pragma once

#include <optional>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/guild#guild-widget-object-guild-widget-structure
    struct GuildWidget {

        bool enabled;

        std::optional<std::string> channel_id;

        GuildWidget() = default;

        GuildWidget(const nlohmann::json &json) {
            enabled = json["enabled"].get<bool>();
            if (!json["channel_id"].is_null()) {
                channel_id = json["channel_id"].get<std::string>();
            }
        }

    };

}