#pragma once

#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/guild#integration-account-object-integration-account-structure
    struct IntegrationAccount {

        std::string id;

        std::string name;

        IntegrationAccount() = default;

        IntegrationAccount(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            name = json["name"].get<std::string>();
        }

    };

}