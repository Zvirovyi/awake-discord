#pragma once

#include <awake-discord/activity-party-size.h>

#include <optional>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/topics/gateway#activity-object-activity-party
    struct ActivityParty {

        std::optional<std::string> id;

        std::optional<ActivityPartySize> size;

        ActivityParty() = default;

        ActivityParty(const nlohmann::json &json) {
            if (json.contains("id")) {
                id = json["id"].get<std::string>();
            }
            if (json.contains("size")) {
                size = ActivityPartySize(json["size"]);
            }
        }

    };

}