#pragma once

#include <awake-discord/user.h>
#include <awake-discord/string-enums.h>
#include <awake-discord/activity.h>
#include <awake-discord/client-status.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/topics/gateway#presence-update-presence-update-event-fields
    struct PresenceUpdate {

        User user;

        std::string guild_id;

        PresenceUpdateStatus status;

        std::vector<Activity> activities;

        ClientStatus client_status;

        PresenceUpdate() = default;

        PresenceUpdate(const nlohmann::json &json) {
            user = User(json["user"]);
            guild_id = json["guild_id"].get<std::string>();
            status = PresenceUpdateStatus_Values::fromString(json["status"].get<std::string>());
            for (const nlohmann::json &jsonEl : json["activities"]) {
                activities.emplace_back(jsonEl);
            }
            client_status = ClientStatus(json["client_status"]);
        }

    };

}