#pragma once

#include <optional>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#embed-object-embed-footer-structure
    struct EmbedFooter {

        std::string text;

        std::optional<std::string> icon_url;

        std::optional<std::string> proxy_icon_url;

        EmbedFooter() = default;

        EmbedFooter(const nlohmann::json &json) {
            text = json["text"].get<std::string>();
            if (json.contains("text")) {
                icon_url = json["icon_url"].get<std::string>();
            }
            if (json.contains("proxy_icon_url")) {
                proxy_icon_url = json["proxy_icon_url"].get<std::string>();
            }
        }

    };

}