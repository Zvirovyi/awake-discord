#pragma once

#include <optional>
#include <string>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#embed-object-embed-thumbnail-structure
    struct EmbedThumbnail {

        std::optional<std::string> url;

        std::optional<std::string> proxy_url;

        std::optional<int> height;

        std::optional<int> width;

        EmbedThumbnail() = default;

        EmbedThumbnail(const nlohmann::json &json) {
            if (json.contains("url")) {
                url = json["url"].get<std::string>();
            }
            if (json.contains("proxy_url")) {
                proxy_url = json["proxy_url"].get<std::string>();
            }
            if (json.contains("height")) {
                height = json["height"].get<int>();
            }
            if (json.contains("width")) {
                width = json["width"].get<int>();
            }
        }

    };

}