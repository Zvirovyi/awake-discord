#pragma once

#include <awake-discord/user.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/guild#ban-object-ban-structure
    struct Ban {

        std::optional<std::string> reason;

        User user;

        Ban() = default;

        Ban(const nlohmann::json &json) {
            if (!json["reason"].is_null()) {
                reason = json["reason"].get<std::string>();
            }
            user = User(json["user"]);
        }

    };

}