#pragma once

#include <optional>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/topics/gateway#activity-object-activity-assets
    struct ActivityAssets {

        std::optional<std::string> large_image;

        std::optional<std::string> large_text;

        std::optional<std::string> small_image;

        std::optional<std::string> small_text;

        ActivityAssets() = default;

        ActivityAssets(const nlohmann::json &json) {
            if (json.contains("large_image")) {
                large_image = json["large_image"].get<std::string>();
            }
            if (json.contains("large_text")) {
                large_text = json["large_text"].get<std::string>();
            }
            if (json.contains("small_image")) {
                small_image = json["small_image"].get<std::string>();
            }
            if (json.contains("small_text")) {
                small_text = json["small_text"].get<std::string>();
            }
        }

    };

}