#pragma once

#include <awake-discord/user.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/emoji#emoji-object-emoji-structure
    struct Emoji {

        std::optional<std::string> id;

        std::optional<std::string> name;

        std::vector<std::string> roles;

        std::optional<User> user;

        std::optional<bool> require_colons;

        std::optional<bool> managed;

        std::optional<bool> animated;

        std::optional<bool> available;

        Emoji() = default;

        Emoji(const nlohmann::json &json) {
            if (!json["id"].is_null()) {
                id = json["id"].get<std::string>();
            }
            if (!json["name"].is_null()) {
                name = json["name"].get<std::string>();
            }
            if (json.contains("roles")) {
                for (const nlohmann::json &jsonEl : json["roles"]) {
                    roles.push_back(jsonEl.get<std::string>());
                }
            }
            if (json.contains("user")) {
                user = User(json["user"]);
            }
            if (json.contains("require_colons")) {
                require_colons = json["require_colons"].get<bool>();
            }
            if (json.contains("managed")) {
                managed = json["managed"].get<bool>();
            }
            if (json.contains("animated")) {
                animated = json["animated"].get<bool>();
            }
            if (json.contains("available")) {
                available = json["available"].get<bool>();
            }
        }

    };

}