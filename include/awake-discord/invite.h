#pragma once

#include <awake-discord/guild.h>
#include <awake-discord/channel.h>
#include <awake-discord/user.h>
#include <awake-discord/application.h>
#include <awake-discord/invite-stage-instance.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/invite#invite-object-invite-structure
    struct Invite {

        std::string code;

        std::optional<Guild> guild;

        Channel channel;

        std::optional<User> inviter;

        std::optional<InviteTargetType> target_type;

        std::optional<User> target_user;

        std::optional<Application> target_application;

        std::optional<int> approximate_presence_count;

        std::optional<int> approximate_member_count;

        std::optional<time_t> expires_at;

        std::optional<InviteStageInstance> stage_instance;

        Invite() = default;

        Invite(const nlohmann::json &json) {
            code = json["code"].get<std::string>();
            if (json.contains("guild")) {
                guild = Guild(json["guild"]);
            }
            channel = Channel(json["channel"]);
            if (json.contains("inviter")) {
                inviter = User(json["inviter"]);
            }
            if (json.contains("target_type")) {
                target_type = InviteTargetType(json["target_type"].get<int>());
            }
            if (json.contains("target_user")) {
                target_user = User(json["target_user"]);
            }
            if (json.contains("target_application")) {
                target_application = Application(json["target_application"]);
            }
            if (json.contains("approximate_presence_count")) {
                approximate_presence_count = json["approximate_presence_count"].get<int>();
            }
            if (json.contains("approximate_member_count")) {
                approximate_member_count = json["approximate_member_count"].get<int>();
            }
            if (json.contains("expires_at") && !json["expires_at"].is_null()) {
                expires_at = ISO8601::fromTimestamp(json["expires_at"].get<std::string>());
            }
            if (json.contains("stage_instance")) {
                stage_instance = InviteStageInstance(json["stage_instance"]);
            }
        }

    };

}