#pragma once

#include <optional>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/topics/gateway#activity-object-activity-secrets
    struct ActivitySecrets {

        std::optional<std::string> join;

        std::optional<std::string> spectate;

        std::optional<std::string> match;

        ActivitySecrets() = default;

        ActivitySecrets(const nlohmann::json &json) {
            if (json.contains("join")) {
                join = json["join"].get<std::string>();
            }
            if (json.contains("spectate")) {
                spectate = json["spectate"].get<std::string>();
            }
            if (json.contains("match")) {
                match = json["match"].get<std::string>();
            }
        }

    };

}