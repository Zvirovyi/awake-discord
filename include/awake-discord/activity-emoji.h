#pragma once

#include <optional>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/topics/gateway#activity-object-activity-emoji
    struct ActivityEmoji {

        std::string name;

        std::optional<std::string> id;

        std::optional<bool> animated;

        ActivityEmoji() = default;

        ActivityEmoji(const nlohmann::json &json) {
            name = json["name"].get<std::string>();
            if (json.contains("id")) {
                id = json["id"].get<std::string>();
            }
            if (json.contains("animated")) {
                animated = json["animated"].get<bool>();
            }
        }

    };

}