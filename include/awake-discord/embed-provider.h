#pragma once

#include <optional>
#include <string>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#embed-object-embed-provider-structure
    struct EmbedProvider {

        std::optional<std::string> name;

        std::optional<std::string> url;

        EmbedProvider() = default;

        EmbedProvider(const nlohmann::json &json) {
            if (json.contains("name")) {
                name = json["name"].get<std::string>();
            }
            if (json.contains("url")) {
                url = json["url"].get<std::string>();
            }
        }

    };

}