#pragma once

#include <awake-discord/user.h>
#include <awake-discord/guild.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/guild-template#guild-template-object-guild-template-structure
    struct GuildTemplate {

        std::string code;

        std::string name;

        std::optional<std::string> description;

        int usage_count;

        std::string creator_id;

        User creator;

        time_t created_at;

        time_t updated_at;

        std::string source_guild_id;

        Guild serialized_source_guild;

        std::optional<bool> is_dirty;

        GuildTemplate() = default;

        GuildTemplate(const nlohmann::json &json) {
            code = json["code"].get<std::string>();
            name = json["name"].get<std::string>();
            if (!json["description"].is_null()) {
                description = json["description"].get<std::string>();
            }
            usage_count = json["usage_count"].get<int>();
            creator_id = json["creator_id"].get<std::string>();
            creator = User(json["creator"]);
            created_at = ISO8601::fromTimestamp(json["created_at"].get<std::string>());
            updated_at = ISO8601::fromTimestamp(json["updated_at"].get<std::string>());
            source_guild_id = json["source_guild_id"].get<std::string>();
            serialized_source_guild = Guild(json["serialized_source_guild"]);
            if (!json["is_dirty"].is_null()) {
                is_dirty = json["is_dirty"].get<bool>();
            }
        }

    };

}