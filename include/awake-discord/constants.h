#pragma once

#include <string>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/reference#api-versioning
    const std::string DiscordAPIUrl { "https://discord.com/api/v9" };

}