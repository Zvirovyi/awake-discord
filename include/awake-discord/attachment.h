#pragma once

#include <optional>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#attachment-object-attachment-structure
    struct Attachment {

        std::string id;

        std::string filename;

        std::optional<std::string> content_type;

        int size;

        std::string url;

        std::string proxy_url;

        std::optional<int> height;

        std::optional<int> width;

        Attachment() = default;

        Attachment(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            filename = json["filename"].get<std::string>();
            if (json.contains("content_type")) {
                content_type = json["content_type"].get<std::string>();
            }
            size = json["size"].get<int>();
            url = json["url"].get<std::string>();
            proxy_url = json["proxy_url"].get<std::string>();
            if (json.contains("height") && !json["height"].is_null()) {
                height = json["height"].get<int>();
            }
            if (json.contains("width") && !json["width"].is_null()) {
                width = json["width"].get<int>();
            }
        }

    };

}