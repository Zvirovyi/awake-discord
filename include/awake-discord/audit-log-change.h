#pragma once

#include <awake-discord/string-enums.h>
#include <awake-discord/enums.h>
#include <awake-discord/role.h>
#include <awake-discord/overwrite.h>

#include <variant>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/audit-log#audit-log-change-object-audit-log-change-key
    typedef std::variant<std::string, int, MFALevel, VerificationLevel, ExplicitContentFilterLevel, DefaultMessageNotificationLevel, std::vector<PartialRole>, bool, std::vector<Overwrite>, PermissionFlags, std::variant<ChannelType, std::string>, IntegrationExpireBehavior, PrivacyLevel> AuditLogChangeValue;

    //https://discord.com/developers/docs/resources/audit-log#audit-log-change-object-audit-log-change-structure
    struct AuditLogChange {

        AuditLogChangeValue new_value;

        AuditLogChangeValue old_value;

        AuditLogChangeKey key;

        AuditLogChange() = default;

        AuditLogChange(const nlohmann::json &json) {
            key = AuditLogChangeKey_Values::fromString(json["key"].get<std::string>());
            switch (key) {
                case AuditLogChangeKey::name:
                case AuditLogChangeKey::description:
                case AuditLogChangeKey::icon_hash:
                case AuditLogChangeKey::splash_hash:
                case AuditLogChangeKey::discovery_splash_hash:
                case AuditLogChangeKey::banner_hash:
                case AuditLogChangeKey::owner_id:
                case AuditLogChangeKey::region:
                case AuditLogChangeKey::preferred_locale:
                case AuditLogChangeKey::afk_channel_id:
                case AuditLogChangeKey::rules_channel_id:
                case AuditLogChangeKey::public_updates_channel_id:
                case AuditLogChangeKey::vanity_url_code:
                case AuditLogChangeKey::prune_delete_days:
                case AuditLogChangeKey::widget_channel_id:
                case AuditLogChangeKey::system_channel_id:
                case AuditLogChangeKey::topic:
                case AuditLogChangeKey::application_id:
                case AuditLogChangeKey::code:
                case AuditLogChangeKey::channel_id:
                case AuditLogChangeKey::inviter_id:
                case AuditLogChangeKey::nick:
                case AuditLogChangeKey::avatar_hash:
                case AuditLogChangeKey::id:
                {
                    if (json.contains("new_value")) {
                        new_value = json["new_value"].get<std::string>();
                    }
                    if (json.contains("old_value")) {
                        old_value = json["old_value"].get<std::string>();
                    }
                    break;
                }
                case AuditLogChangeKey::afk_timeout:
                case AuditLogChangeKey::position:
                case AuditLogChangeKey::bitrate:
                case AuditLogChangeKey::rate_limit_per_user:
                case AuditLogChangeKey::color:
                case AuditLogChangeKey::max_uses:
                case AuditLogChangeKey::uses:
                case AuditLogChangeKey::max_age:
                case AuditLogChangeKey::expire_grace_period:
                case AuditLogChangeKey::user_limit:
                {
                    if (json.contains("new_value")) {
                        new_value = json["new_value"].get<int>();
                    }
                    if (json.contains("old_value")) {
                        old_value = json["old_value"].get<int>();
                    }
                    break;
                }
                case AuditLogChangeKey::mfa_level:
                {
                    if (json.contains("new_value")) {
                        new_value = MFALevel(json["new_value"].get<int>());
                    }
                    if (json.contains("old_value")) {
                        old_value = MFALevel(json["old_value"].get<int>());
                    }
                    break;
                }
                case AuditLogChangeKey::verification_level:
                {
                    if (json.contains("new_value")) {
                        new_value = VerificationLevel(json["new_value"].get<int>());
                    }
                    if (json.contains("old_value")) {
                        old_value = VerificationLevel(json["old_value"].get<int>());
                    }
                    break;
                }
                case AuditLogChangeKey::explicit_content_filter:
                {
                    if (json.contains("new_value")) {
                        new_value = ExplicitContentFilterLevel(json["new_value"].get<int>());
                    }
                    if (json.contains("old_value")) {
                        old_value = ExplicitContentFilterLevel(json["old_value"].get<int>());
                    }
                    break;
                }
                case AuditLogChangeKey::default_message_notifications:
                {
                    if (json.contains("new_value")) {
                        new_value = DefaultMessageNotificationLevel(json["new_value"].get<int>());
                    }
                    if (json.contains("old_value")) {
                        old_value = DefaultMessageNotificationLevel(json["old_value"].get<int>());
                    }
                    break;
                }
                case AuditLogChangeKey::$add:
                case AuditLogChangeKey::$remove:
                {
                    if (json.contains("new_value")) {
                        std::vector<PartialRole> roles;
                        for (const nlohmann::json &jsonEl : json["new_value"]) {
                            roles.emplace_back(jsonEl);
                        }
                        new_value = roles;
                    }
                    if (json.contains("old_value")) {
                        std::vector<PartialRole> roles;
                        for (const nlohmann::json &jsonEl : json["old_value"]) {
                            roles.emplace_back(jsonEl);
                        }
                        old_value = roles;
                    }
                    break;
                }
                case AuditLogChangeKey::widget_enabled:
                case AuditLogChangeKey::nsfw:
                case AuditLogChangeKey::hoist:
                case AuditLogChangeKey::mentionable:
                case AuditLogChangeKey::temporary:
                case AuditLogChangeKey::deaf:
                case AuditLogChangeKey::mute:
                case AuditLogChangeKey::enable_emoticons:
                {
                    if (json.contains("new_value")) {
                        new_value = MFALevel(json["new_value"].get<bool>());
                    }
                    if (json.contains("old_value")) {
                        old_value = MFALevel(json["old_value"].get<bool>());
                    }
                    break;
                }
                case AuditLogChangeKey::permission_overwrites:
                {
                    if (json.contains("new_value")) {
                        std::vector<Overwrite> overwrites;
                        for (const nlohmann::json &jsonEl : json["new_value"]) {
                            overwrites.emplace_back(jsonEl);
                        }
                        new_value = overwrites;
                    }
                    if (json.contains("old_value")) {
                        std::vector<Overwrite> overwrites;
                        for (const nlohmann::json &jsonEl : json["old_value"]) {
                            overwrites.emplace_back(jsonEl);
                        }
                        old_value = overwrites;
                    }
                    break;
                }
                case AuditLogChangeKey::permissions:
                case AuditLogChangeKey::allow:
                case AuditLogChangeKey::deny:
                {
                    if (json.contains("new_value")) {
                        new_value = PermissionFlags(std::stoll(json["new_value"].get<std::string>()));
                    }
                    if (json.contains("old_value")) {
                        old_value = PermissionFlags(std::stoll(json["old_value"].get<std::string>()));
                    }
                    break;
                }
                case AuditLogChangeKey::type:
                {
                    if (json.contains("new_value")) {
                        std::variant<ChannelType, std::string> res;
                        if (json["new_value"].is_number_integer()) {
                            res = ChannelType(json["new_value"].get<int>());
                        } else {
                            res = json["new_value"].get<std::string>();
                        }
                        new_value = res;
                    }
                    if (json.contains("old_value")) {
                        std::variant<ChannelType, std::string> res;
                        if (json["old_value"].is_number_integer()) {
                            res = ChannelType(json["old_value"].get<int>());
                        } else {
                            res = json["old_value"].get<std::string>();
                        }
                        old_value = res;
                    }
                    break;
                }
                case AuditLogChangeKey::expire_behavior:
                {
                    if (json.contains("new_value")) {
                        new_value = IntegrationExpireBehavior(json["new_value"].get<int>());
                    }
                    if (json.contains("old_value")) {
                        old_value = IntegrationExpireBehavior(json["old_value"].get<int>());
                    }
                    break;
                }
                case AuditLogChangeKey::privacy_level:
                {
                    if (json.contains("new_value")) {
                        new_value = PrivacyLevel(json["new_value"].get<int>());
                    }
                    if (json.contains("old_value")) {
                        old_value = PrivacyLevel(json["old_value"].get<int>());
                    }
                    break;
                }
            }
        }

    };

}