#pragma once

#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/voice#voice-region-object-voice-region-structure
    struct VoiceRegion {

        std::string id;

        std::string name;

        bool vip;

        bool optimal;

        bool deprecated;

        bool custom;

        VoiceRegion() = default;

        VoiceRegion(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            name = json["name"].get<std::string>();
            vip = json["vip"].get<bool>();
            optimal = json["optimal"].get<bool>();
            deprecated = json["deprecated"].get<bool>();
            custom = json["custom"].get<bool>();
        }

    };

}