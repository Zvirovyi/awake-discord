#pragma once

#include <awake-discord/welcome-screen-channel.h>

#include <vector>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/guild#welcome-screen-object-welcome-screen-structure
    struct WelcomeScreen {

        std::optional<std::string> description;

        std::vector<WelcomeScreenChannel> welcome_channels;

        WelcomeScreen() = default;

        WelcomeScreen(const nlohmann::json &json) {
            if (!json["description"].is_null()) {
                description = json["description"].get<std::string>();
            }
            for (const nlohmann::json &jsonEl : json["welcome_channels"]) {
                welcome_channels.emplace_back(jsonEl);
            }
        }

    };

}