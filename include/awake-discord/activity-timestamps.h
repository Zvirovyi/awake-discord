#pragma once

#include <optional>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/topics/gateway#activity-object-activity-timestamps
    struct ActivityTimestamps {

        std::optional<time_t> start;

        std::optional<time_t> end;

        ActivityTimestamps() = default;

        ActivityTimestamps(const nlohmann::json &json) {
            if (json.contains("start")) {
                start = json["start"].get<int>();
            }
            if (json.contains("end")) {
                end = json["end"].get<int>();
            }
        }

    };

}