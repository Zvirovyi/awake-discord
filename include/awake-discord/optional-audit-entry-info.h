#pragma once

#include <awake-discord/string-enums.h>

#include <nlohmann/json.hpp>
#include <optional>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/audit-log#audit-log-entry-object-optional-audit-entry-info
    //TODO can convert string to int?
    //TODO this is always partial object, so making all fields optional
    struct OptionalAuditEntryInfo {

        std::optional<std::string> delete_member_days;

        std::optional<std::string> members_removed;

        std::optional<std::string> channel_id;

        std::optional<std::string> message_id;

        std::optional<std::string> count;

        std::optional<std::string> id;

        std::optional<OptionalAuditEntryInfoType> type;

        std::optional<std::string> role_name;

        OptionalAuditEntryInfo() = default;

        OptionalAuditEntryInfo(const nlohmann::json &json) {
            if (json.contains("delete_member_days")) {
                delete_member_days = json["delete_member_days"].get<std::string>();
            }
            if (json.contains("members_removed")) {
                members_removed = json["members_removed"].get<std::string>();
            }
            if (json.contains("channel_id")) {
                channel_id = json["channel_id"].get<std::string>();
            }
            if (json.contains("message_id")) {
                message_id = json["message_id"].get<std::string>();
            }
            if (json.contains("count")) {
                count = json["count"].get<std::string>();
            }
            if (json.contains("id")) {
                id = json["id"].get<std::string>();
            }
            if (json.contains("type")) {
                type = OptionalAuditEntryInfoType_Values::fromString(json["type"].get<std::string>());
            }
            if (json.contains("role_name")) {
                role_name = json["role_name"].get<std::string>();
            }
        }

    };

}