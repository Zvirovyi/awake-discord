#pragma once

#include <optional>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#embed-object-embed-field-structure
    struct EmbedField {

        std::string name;

        std::string value;

        std::optional<bool> inline_;

        EmbedField() = default;

        EmbedField(const nlohmann::json &json) {
            name = json["name"].get<std::string>();
            value = json["value"].get<std::string>();
            if (json.contains("inline")) {
                inline_ = json["inline"].get<bool>();
            }
        }

    };

}