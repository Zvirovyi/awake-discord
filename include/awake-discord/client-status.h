#pragma once

#include <awake-discord/string-enums.h>

#include <optional>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/topics/gateway#client-status-object
    struct ClientStatus {

        std::optional<ClientStatusStatus> desktop;

        std::optional<ClientStatusStatus> mobile;

        std::optional<ClientStatusStatus> web;

        ClientStatus() = default;

        ClientStatus(const nlohmann::json &json) {
            if (json.contains("desktop")) {
                desktop = ClientStatusStatus_Values::fromString(json["desktop"].get<std::string>());
            }
            if (json.contains("mobile")) {
                mobile = ClientStatusStatus_Values::fromString(json["mobile"].get<std::string>());
            }
            if (json.contains("web")) {
                web = ClientStatusStatus_Values::fromString(json["web"].get<std::string>());
            }
        }

    };

}