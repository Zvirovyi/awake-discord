#pragma once

#include <optional>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#embed-object-embed-author-structure
    struct EmbedAuthor {

        std::optional<std::string> name;

        std::optional<std::string> url;

        std::optional<std::string> icon_url;

        std::optional<std::string> proxy_icon_url;

        EmbedAuthor() = default;

        EmbedAuthor(const nlohmann::json &json) {
            if (json.contains("name")) {
                name = json["name"].get<std::string>();
            }
            if (json.contains("url")) {
                url = json["url"].get<std::string>();
            }
            if (json.contains("icon_url")) {
                icon_url = json["icon_url"].get<std::string>();
            }
            if (json.contains("proxy_icon_url")) {
                proxy_icon_url = json["proxy_icon_url"].get<std::string>();
            }
        }

    };

}