#pragma once

#include <awake-discord/user.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#message-object-message-sticker-structure
    struct MessageSticker {

        std::string id;

        std::optional<std::string> pack_id;

        std::string name;

        std::string description;

        std::string tags;

        MessageStickerFormatType format_type;

        std::optional<bool> available;

        std::optional<std::string> guild_id;

        std::optional<User> user;

        std::optional<int> sort_value;

        MessageSticker() = default;

        MessageSticker(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            if (json.contains("pack_id")) {
                pack_id = json["pack_id"].get<std::string>();
            }
            name = json["name"].get<std::string>();
            description = json["description"].get<std::string>();
            tags = json["tags"].get<std::string>();
            format_type = MessageStickerFormatType(json["format_type"].get<int>());
            if (json.contains("available")) {
                available = json["available"].get<bool>();
            }
            if (json.contains("guild_id")) {
                guild_id = json["guild_id"].get<std::string>();
            }
            if (json.contains("user")) {
                user = User(json["user"]);
            }
            if (json.contains("sort_value")) {
                sort_value = json["sort_value"].get<int>();
            }
        }

    };

}