#pragma once

#include <awake-discord/enums.h>

#include <optional>
#include <nlohmann/json.hpp>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/user#user-object-user-structure
    struct User {

        std::string id;

        std::string username;

        std::string discriminator;

        std::optional<std::string> avatar;

        std::optional<bool> bot;

        std::optional<bool> system;

        std::optional<bool> mfa_enabled;

        std::optional<std::string> locale;

        std::optional<bool> verified;

        std::optional<std::string> email;

        std::optional<UserFlags> flags;

        std::optional<PremiumType> premium_type;

        std::optional<UserFlags> public_flags;

        User() = default;

        User(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            username = json["username"].get<std::string>();
            discriminator = json["discriminator"].get<std::string>();
            if (!json["avatar"].is_null()) {
                avatar = json["avatar"].get<std::string>();
            }
            if (json.contains("bot")) {
                bot = json["bot"].get<bool>();
            }
            if (json.contains("system")) {
                system = json["system"].get<bool>();
            }
            if (json.contains("mfa_enabled")) {
                mfa_enabled = json["mfa_enabled"].get<bool>();
            }
            if (json.contains("locale")) {
                locale = json["locale"].get<std::string>();
            }
            if (json.contains("verified")) {
                verified = json["verified"].get<bool>();
            }
            if (json.contains("email") && !json["email"].is_null()) {
                email = json["email"].get<std::string>();
            }
            if (json.contains("flags")) {
                flags = json["flags"].get<int>();
            }
            if (json.contains("premium_type")) {
                premium_type = PremiumType { json["premium_type"].get<int>() };
            }
            if (json.contains("public_flags")) {
                public_flags = json["public_flags"].get<int>();
            }
        }

    };

}