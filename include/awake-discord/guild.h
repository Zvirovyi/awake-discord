#pragma once

#include <awake-discord/presence-update.h>
#include <awake-discord/role.h>
#include <awake-discord/string-enums.h>
#include <awake-discord/voice-state.h>
#include <awake-discord/channel.h>
#include <awake-discord/welcome-screen.h>
#include <awake-discord/stage-instance.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/guild#guild-object-guild-structure
    struct Guild {

        std::string id;

        std::string name;

        std::optional<std::string> icon;

        std::optional<std::string> icon_hash;

        std::optional<std::string> splash;

        std::optional<std::string> discovery_splash;

        std::optional<bool> owner;

        std::string owner_id;

        PermissionFlags permissions;

        std::optional<std::string> region;

        std::optional<std::string> afk_channel_id;

        int afk_timeout;

        std::optional<bool> widget_enabled;

        std::optional<std::string> widget_channel_id;

        VerificationLevel verification_level;

        DefaultMessageNotificationLevel default_message_notifications;

        ExplicitContentFilterLevel explicit_content_filter;

        std::vector<Role> roles;

        std::vector<Emoji> emojis;

        GuildFeatureFlags features;

        MFALevel mfa_level;

        std::optional<std::string> application_id;

        std::optional<std::string> system_channel_id;

        SystemChannelFlags system_channel_flags;

        std::optional<std::string> rules_channel_id;

        std::optional<time_t> joined_at;

        std::optional<bool> large;

        std::optional<bool> unavailable;

        std::optional<int> member_count;

        std::vector<VoiceState> voice_states;

        std::vector<GuildMember> members;

        std::vector<Channel> channels;

        std::vector<Channel> threads;

        std::vector<PresenceUpdate> presences;

        std::optional<int> max_presences;

        std::optional<int> max_members;

        std::optional<std::string> vanity_url_code;

        std::optional<std::string> description;

        std::optional<std::string> banner;

        PremiumTier premium_tier;

        std::optional<int> premium_subscription_count;

        std::string preferred_locale;

        std::optional<std::string> public_updates_channel_id;

        std::optional<int> max_video_channel_users;

        std::optional<int> approximate_member_count;

        std::optional<int> approximate_presence_count;

        std::optional<WelcomeScreen> welcome_screen;

        GuildNSFWLevel nsfw_level;

        std::vector<StageInstance> stage_instances;

        Guild() = default;

        Guild(const nlohmann::json &json) {
            id = json["id"].get<std::string>();
            name = json["name"].get<std::string>();
            if (!json["icon"].is_null()) {
                icon = json["icon"].get<std::string>();
            }
            if (json.contains("icon_hash") && !json["icon_hash"].is_null()) {
                icon_hash = json["icon_hash"].get<std::string>();
            }
            if (!json["splash"].is_null()) {
                splash = json["splash"].get<std::string>();
            }
            if (!json["discovery_splash"].is_null()) {
                discovery_splash = json["discovery_splash"].get<std::string>();
            }
            if (json.contains("owner")) {
                owner = json["owner"].get<bool>();
            }
            owner_id = json["owner_id"].get<std::string>();
            permissions = PermissionFlags(std::stoll(json["permissions"].get<std::string>()));
            if (json.contains("region") && !json["region"].is_null()) {
                region = json["region"].get<std::string>();
            }
            if (!json["afk_channel_id"].is_null()) {
                afk_channel_id = json["afk_channel_id"].get<std::string>();
            }
            afk_timeout = json["afk_timeout"].get<int>();
            if (json.contains("widget_enabled")) {
                widget_enabled = json["widget_enabled"].get<bool>();
            }
            if (json.contains("widget_channel_id") && !json["widget_channel_id"].is_null()) {
                widget_channel_id = json["widget_channel_id"].get<std::string>();
            }
            verification_level = VerificationLevel(json["verification_level"].get<int>());
            default_message_notifications = DefaultMessageNotificationLevel(json["default_message_notifications"].get<int>());
            explicit_content_filter = ExplicitContentFilterLevel(json["explicit_content_filter"].get<int>());
            for (const nlohmann::json &jsonEl : json["roles"]) {
                roles.emplace_back(jsonEl);
            }
            for (const nlohmann::json &jsonEl : json["emojis"]) {
                emojis.emplace_back(jsonEl);
            }
            std::vector<std::string> featuresValues;
            for (const nlohmann::json &jsonEl : json["features"]) {
                featuresValues.emplace_back(jsonEl.get<std::string>());
            }
            features = GuildFeatureFlags_Values::fromStringsVector(featuresValues);
            mfa_level = MFALevel(json["mfa_level"].get<int>());
            if (!json["application_id"].is_null()) {
                application_id = json["application_id"].get<std::string>();
            }
            if (!json["system_channel_id"].is_null()) {
                system_channel_id = json["system_channel_id"].get<std::string>();
            }
            system_channel_flags = SystemChannelFlags(json["system_channel_flags"].get<int>());
            if (!json["rules_channel_id"].is_null()) {
                rules_channel_id = json["rules_channel_id"].get<std::string>();
            }
            if (json.contains("joined_at")) {
                joined_at = ISO8601::fromTimestamp(json["joined_at"].get<std::string>());
            }
            if (json.contains("large")) {
                large = json["large"].get<bool>();
            }
            if (json.contains("unavailable")) {
                unavailable = json["unavailable"].get<bool>();
            }
            if (json.contains("member_count")) {
                member_count = json["member_count"].get<int>();
            }
            if (json.contains("voice_states")) {
                for (const nlohmann::json &jsonEl : json["voice_states"]) {
                    voice_states.emplace_back(jsonEl);
                }
            }
            if (json.contains("members")) {
                for (const nlohmann::json &jsonEl : json["members"]) {
                    members.emplace_back(jsonEl);
                }
            }
            if (json.contains("channels")) {
                for (const nlohmann::json &jsonEl : json["channels"]) {
                    channels.emplace_back(jsonEl);
                }
            }
            if (json.contains("threads")) {
                for (const nlohmann::json &jsonEl : json["threads"]) {
                    threads.emplace_back(jsonEl);
                }
            }
            if (json.contains("presences")) {
                for (const nlohmann::json &jsonEl : json["presences"]) {
                    presences.emplace_back(jsonEl);
                }
            }
            if (json.contains("max_presences") && !json["max_presences"].is_null()) {
                max_presences = json["max_presences"].get<int>();
            }
            if (json.contains("max_members")) {
                max_members = json["max_members"].get<int>();
            }
            if (!json["vanity_url_code"].is_null()) {
                vanity_url_code = json["vanity_url_code"].get<std::string>();
            }
            if (!json["description"].is_null()) {
                description = json["description"].get<std::string>();
            }
            if (!json["banner"].is_null()) {
                banner = json["banner"].get<std::string>();
            }
            premium_tier = PremiumTier(json["premium_tier"].get<int>());
            if (json.contains("premium_subscription_count")) {
                premium_subscription_count = json["premium_subscription_count"].get<int>();
            }
            preferred_locale = json["preferred_locale"].get<std::string>();
            if (!json["public_updates_channel_id"].is_null()) {
                public_updates_channel_id = json["public_updates_channel_id"].get<std::string>();
            }
            if (json.contains("max_video_channel_users")) {
                max_video_channel_users = json["max_video_channel_users"].get<int>();
            }
            if (json.contains("approximate_member_count")) {
                approximate_member_count = json["approximate_member_count"].get<int>();
            }
            if (json.contains("approximate_presence_count")) {
                approximate_presence_count = json["approximate_presence_count"].get<int>();
            }
            if (json.contains("welcome_screen")) {
                welcome_screen = WelcomeScreen(json["welcome_screen"]);
            }
            nsfw_level = GuildNSFWLevel(json["nsfw_level"].get<int>());
            if (json.contains("stage_instances")) {
                for (const nlohmann::json &jsonEl : json["stage_instances"]) {
                    stage_instances.emplace_back(jsonEl);
                }
            }
        }

    };

}