#pragma once

#include <awake-discord/guild-member.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/invite#invite-stage-instance-object-invite-stage-instance-structure
    struct InviteStageInstance {

        std::vector<GuildMember> members;

        int participant_count;

        int speaker_count;

        std::string topic;

        InviteStageInstance() = default;

        InviteStageInstance(const nlohmann::json &json) {
            for (const nlohmann::json &jsonEl : json["members"]) {
                members.emplace_back(jsonEl);
            }
            participant_count = json["participant_count"].get<int>();
            speaker_count = json["speaker_count"].get<int>();
            topic = json["topic"].get<std::string>();
        }

    };

}