#pragma once

#include <awake-discord/audit-log-change.h>
#include <awake-discord/optional-audit-entry-info.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/audit-log#audit-log-entry-object-audit-log-entry-structure
    struct AuditLogEntry {

        std::optional<std::string> target_id;

        std::vector<AuditLogChange> changes;

        std::optional<std::string> user_id;

        std::string id;

        AuditLogEvent action_type;

        std::optional<OptionalAuditEntryInfo> options;

        std::optional<std::string> reason;

        AuditLogEntry() = default;

        AuditLogEntry(const nlohmann::json &json) {
            if (!json["target_id"].is_null()) {
                target_id = json["target_id"].get<std::string>();
            }
            if (json.contains("changes")) {
                for (const nlohmann::json &jsonEl : json["changes"]) {
                    changes.emplace_back(jsonEl);
                }
            }
            if (!json["user_id"].is_null()) {
                user_id = json["user_id"].get<std::string>();
            }
            id = json["id"].get<std::string>();
            action_type = AuditLogEvent(json["action_type"].get<int>());
            if (json.contains("options")) {
                options = OptionalAuditEntryInfo(json["options"]);
            }
            if (json.contains("reason")) {
                reason = json["reason"].get<std::string>();
            }
        }

    };

}