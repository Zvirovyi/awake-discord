#pragma once

#include <optional>
#include <string>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/channel#message-reference-object-message-reference-structure
    struct MessageReference {

        std::optional<std::string> message_id;

        std::optional<std::string> channel_id;

        std::optional<std::string> guild_id;

        std::optional<bool> fail_if_not_exists;

        MessageReference() = default;

        MessageReference(const nlohmann::json &json) {
            if (json.contains("message_id")) {
                message_id = json["message_id"].get<std::string>();
            }
            if (json.contains("channel_id")) {
                channel_id = json["channel_id"].get<std::string>();
            }
            if (json.contains("guild_id")) {
                guild_id = json["guild_id"].get<std::string>();
            }
            if (json.contains("fail_if_not_exists")) {
                fail_if_not_exists = json["fail_if_not_exists"].get<bool>();
            }
        }

    };

}