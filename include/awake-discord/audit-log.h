#pragma once

#include <awake-discord/webhook.h>
#include <awake-discord/audit-log-entry.h>
#include <awake-discord/integration.h>

namespace AwakeDiscord {

    //https://discord.com/developers/docs/resources/audit-log#audit-log-object-audit-log-structure
    struct AuditLog {

        std::vector<Webhook> webhooks;

        std::vector<User> users;

        std::vector<AuditLogEntry> audit_log_entries;

        std::vector<Integration> integrations;

        AuditLog() = default;

        AuditLog(const nlohmann::json &json) {
            for (const nlohmann::json &jsonEl : json["webhooks"]) {
                webhooks.emplace_back(jsonEl);
            }
            for (const nlohmann::json &jsonEl : json["users"]) {
                users.emplace_back(jsonEl);
            }
            for (const nlohmann::json &jsonEl : json["audit_log_entries"]) {
                audit_log_entries.emplace_back(jsonEl);
            }
            for (const nlohmann::json &jsonEl : json["integrations"]) {
                integrations.emplace_back(jsonEl);
            }
        }

    };

}