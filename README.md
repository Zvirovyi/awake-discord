# Archived

This project uncompleted and will not be such, because of Discord have many troubles with their documentation(maybe dev-driven approach), that requiring huge amount of resources to maintain library. I've commited last changes made by me and archived work.

# Awake Discord

C++ library for Discord API, that aiming to provide fully v9 compatibility.

## Important

Currently this lib is in heavy development and not completed, that's why i recommend to use the dev branch now.

Overall TODO list:

- [x] Classes definition
- [ ] HTTP API implementation
- [ ] WebSocket API implementation
- [ ] Documentation
- [ ] Examples

## How to use?

Simply connect this lib in CMake using FetchContent(requires cmake 3.11).

Example cmake script:

```cmake
cmake_minimum_required(VERSION 3.11)
project(AwakeDiscordTest)
include(FetchContent)

set(CMAKE_CXX_STANDARD 20)

FetchContent_Declare(
        awake-discord
        GIT_REPOSITORY https://gitlab.com/NicholasRush/awake-discord.git
        GIT_TAG origin/dev
)
FetchContent_MakeAvailable(awake-discord)

add_executable(AwakeDiscordTest main.cpp)

target_link_libraries(AwakeDiscordTest PUBLIC awake-discord)
```

## Used

- [CPR](https://github.com/whoshuu/cpr.git)
- [Nlohmann's JSON](https://github.com/nlohmann/json)
- [bitflags](https://github.com/m-peko/bitflags)